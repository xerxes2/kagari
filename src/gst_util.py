import langcodes

def process_disc_info(disc_info, video_list=[], audio_list=[], sub_list=[], stream_list=[]):
  _tags = disc_info.get_stream_info().get_tags()
  if _tags:
    if _tags.get_string("title")[1]:
      media_title = _tags.get_string("title")[1]
    else:
      media_title = ""
    container = _tags.get_string("container-format")[1]
  else:
    media_title = ""
    container = ""
  g_str= """# General\n\tTitle: {0}\n\tContainer: {1}\n\tDuration: {2}\n""".format(media_title, container, convert_nanos(disc_info.get_duration()))
  #print(disc_info.get_stream_list())
  _streams = disc_info.get_video_streams()
  if _streams:
    v_str = "# Video\n"
  else:
    v_str = ""
  for i in _streams:
    _tags = i.get_tags()
    _caps = i.get_caps()
    _rate = i.get_bitrate()
    if _rate:
      bit_str = "{0:.1f} kb/s".format(_rate/1000)
    else:
      bit_str = ""
    _str = """\tStream: {0}\n\tCodec: {1}\n\tResolution: {2}x{3}\n\tBitrate: {4}\n\tFramerate: {5:.2f}\n\tBit depth: {6}\n""".format(i.get_stream_number(),
      _tags.get_string("video-codec")[1], i.get_width(), i.get_height(), bit_str, i.get_framerate_num()/i.get_framerate_denom(),
      _caps.get_structure(0).get_value("bit-depth-chroma"))
    v_str = v_str + _str
    video_list.append("{0} {1}x{2}".format(_tags.get_string("video-codec")[1], i.get_width(), i.get_height()))
    stream_list.append("{0}. Video {1} {2}x{3}".format(i.get_stream_number(), _tags.get_string("video-codec")[1], i.get_width(), i.get_height()))
    #print(i.get_tags().to_string().replace(', ', '\n\t'))
    #print(i.get_caps().to_string().replace(', ', '\n\t'))

  _streams = disc_info.get_audio_streams()
  if _streams:
    a_str = "# Audio\n"
  else:
    a_str = ""
  for i in _streams:
    _tags = i.get_tags()
    _codec = _tags.get_string("audio-codec")[1]
    if _codec.endswith("audio"):
      _codec = _codec.rsplit(" ", 1)[0]
    _rate = i.get_bitrate()
    if _rate:
      bit_str = "{0:.1f} kb/s".format(_rate/1000)
    else:
      bit_str = ""
    _lang = i.get_language()
    if not _lang or _lang == "und":
      lang_display = ""
    else:
      lang_display = langcodes.Language.get(_lang).display_name()
    _str = """\tStream: {0}\n\tCodec: {1}\n\tChannels: {2}\n\tBitrate: {3}\n\tSamplerate: {4:.1f} kHz\n\tLanguage: {5}\n""".format(i.get_stream_number(),
      _codec, i.get_channels(), bit_str, i.get_sample_rate()/1000, lang_display)
    a_str = a_str + _str
    audio_list.append("{0} {1}-ch {2}".format(_codec, i.get_channels(), lang_display))
    stream_list.append("{0}. Audio {1} {2}-ch {3}".format(i.get_stream_number(), _codec, i.get_channels(), lang_display))

  _streams = disc_info.get_subtitle_streams()
  if _streams:
    s_str = "# Subtitle\n"
  else:
    s_str = ""
  for i in _streams:
    _lang = i.get_language()
    if not _lang:
      _lang = "und"
    lang_display = langcodes.Language.get(_lang).display_name()
    _str = """\tStream: {0}\n\tLanguage: {1}\n""".format(i.get_stream_number(), lang_display)
    s_str = s_str + _str
    sub_list.append("{0}".format(lang_display))
    stream_list.append("{0}. Subtitle {1}".format(i.get_stream_number(), lang_display))

  media_info = g_str + v_str + a_str + s_str
  return media_info, media_title

def add_sub(sub_list, _lang="und"):
  sub_list.insert(0, langcodes.Language.get(_lang).display_name())

def convert_nanos(t):
    s,nanos = divmod(t, 1000000000)
    m,s = divmod(s, 60)
    if m < 60:
      return "%02i:%02i" %(m,s)
    else:
      h,m = divmod(m, 60)
      return "%i:%02i:%02i" %(h,m,s)

