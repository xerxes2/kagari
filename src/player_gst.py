import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk
gi.require_version('Gst', '1.0')
from gi.repository import Gst

class Main:
  def __init__(self):
    self.player = Gst.ElementFactory.make("playbin", "video-player")
    self.videosink = Gst.ElementFactory.make("gtk4paintablesink", None)
    self.player.set_property("video-sink", self.videosink)
    self.view = Gtk.Picture.new_for_paintable(self.videosink.get_property("paintable"))
    self.player_bus = self.player.get_bus()
    self.player_bus.add_watch(2, self.bus_callback, None)

  def stop(self):
    self.player.set_state(Gst.State.NULL)

  def play(self, file_path, sub_path):
    self.player.set_property('uri', "file://" + file_path)
    self.player.set_property('suburi', "file://" + sub_path)
    self.player.set_state(Gst.State.PLAYING)

  def pause(self, _bool):
    if _bool: self.player.set_state(Gst.State.PAUSED)
    else: self.player.set_state(Gst.State.PLAYING)

  def seek(self, _pos):
    self.player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, _pos)

  def get_position(self):
    return self.player.query_position(Gst.Format.TIME)[1]

  def get_duration(self):
    return self.player.query_duration(Gst.Format.TIME)[1]

  def get_end_of_stream(self):
    return self.end_of_stream

  def show_subtitle(self, _bool):
    self.modify_flags("text", _bool)

  def set_subtitle(self, _int):
    self.player.set_property("current-text", _int)

  def get_sub_tot(self):
    return self.player.get_property("n-text")

  def set_mute(self, _bool):
    self.player.set_property("mute", _bool)

  def set_audio(self, _int):
    self.player.set_property("current-audio", _int)

  def set_video(self, _int):
    self.player.set_property("current-video", _int)

  def force_software(self, _bool):
    self.modify_flags("force-sw-decoders", _bool)

  def modify_flags(self, _flag, _bool):
    _list = self.player.get_property("flags").value_nicks
    if _bool:
      _list.append(_flag)
    else:
      if _flag in _list:
        _list.remove(_flag)
    Gst.util_set_object_arg(self.player, "flags", "+".join(_list))

  def bus_callback(self, _bus, _message, _3=None):
    _type = _message.type
    if _type == Gst.MessageType.EOS:
      self.end_of_stream = True
    return True

