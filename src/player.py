import sys, os, gi
from gi.repository import GLib
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk
gi.require_version('Gst', '1.0')
from gi.repository import Gst
gi.require_version('GstPbutils', '1.0')
from gi.repository import GstPbutils
Gst.init(sys.argv)
from . import gst_util

class Main():
  def __init__(self, window, config, time_label_0, time_label_1, adj, center_box, info_view, video_view, audio_view, sub_view, title):
    super().__init__()
    self.window = window
    self.config = config
    self.time_label_0 = time_label_0
    self.time_label_1 = time_label_1
    self.adj = adj
    self.popup = center_box
    self.info_view = info_view
    self.video_view = video_view
    self.audio_view = audio_view
    self.sub_view = sub_view
    self.title = title
    self.timeout_set_pos = None
    self.timeout_play = None
    self.timeout_cursor = None
    self.popup_has_pointer = False
    self.switch_to_fullscreen = False
    self.pointer_x = 0
    self.seek_bool = False
    self.update_adj = True
    self.play_mode = [0, 0]
    self.forward_seek = self.config.getint("options", "forward") * 1000000000
    self.backward_seek = self.config.getint("options", "backward") * 1000000000
    self.forward_seek_long = self.config.getint("options", "forward-long") * 1000000000
    self.backward_seek_long = self.config.getint("options", "backward-long") * 1000000000
    self.forward_scroll = self.config.getint("options", "forward-scroll") * 1000000000
    self.backward_scroll = self.config.getint("options", "backward-scroll") * 1000000000
    self.forward_side = self.config.getint("options", "forward-side") * 1000000000
    self.backward_side = self.config.getint("options", "backward-side") * 1000000000
    self.popup_time = self.config.getint("options", "popup-time")
    self.disc = GstPbutils.Discoverer.new(2000000000)
    self.disc.connect('discovered', self.disc_callback)
    self.sub_list = []
    self.audio_list = []
    self.video_list = []

    controller = Gtk.GestureClick.new()
    controller.set_button(0)
    controller.connect("pressed", self.button_callback)
    self.view.add_controller(controller)
    controller = Gtk.EventControllerMotion.new()
    controller.connect("motion", self.cursor_show)
    self.view.add_controller(controller)
    controller = Gtk.EventControllerScroll.new(Gtk.EventControllerScrollFlags(1))
    controller.connect("scroll", self.player_window_scroll)
    self.view.add_controller(controller)
    controller = Gtk.EventControllerMotion.new()
    controller.connect("enter", self.popup_enter)
    controller.connect("leave", self.popup_leave)
    self.popup.add_controller(controller)
    controller = Gtk.EventControllerKey.new()
    controller.connect("key-pressed", self.key_press)
    self.view.add_controller(controller)
    self.view.set_focusable(True)

  def player_stop(self):
    if self.timeout_set_pos:
      GLib.source_remove(self.timeout_set_pos)
      self.timeout_set_pos = None
    self.stop()
    self.disc.stop()
    self.media_info = ""
    self.media_title = ""
    self.file_path = ""
    self.file_name = ""
    self.sub_path = ""
    self.sub_list.clear()
    self.audio_list.clear()
    self.video_list.clear()
    self.set_play_mode(0, 0)
    self.set_play_mode(1, 0)
    self.duration = None
    self.dur_str = " / 00:00"
    self.adj.set_value(0.0)
    self.window.lookup_action("pause").set_state(GLib.Variant.new_boolean(False))
    self.show_subtitle(self.window.lookup_action("show-sub").get_state().get_boolean())
    self.set_all()
    self.ready_to_play = True

  def player_play(self, file_path, sub_path=""):
    if self.ready_to_play:
      self.ready_to_play = False
      self.end_of_stream = False
      self.set_play_mode(0, 1)
      self.file_path = file_path
      self.file_name = os.path.basename(file_path)
      self.sub_path = sub_path
      self.play_thread_int = 0
      if not self.timeout_play:
        self.timeout_play = GLib.timeout_add(100, self.player_play_thread)

  def player_play_thread(self):
    self.play_thread_int +=1
    if self.play_thread_int > 4:
      if self.get_play_mode(0) == 1:
        self.disc.start()
        self.disc.discover_uri_async("file://" + self.file_path)
        self.timeout_set_pos = GLib.timeout_add_seconds(1, self.set_position)
        GLib.timeout_add(280, self.set_duration)
      self.timeout_play = None
      return
    return True

  def player_pause(self, _state):
    if self.get_play_mode(1):
      if _state: self.set_play_mode(0, 2)
      else: self.set_play_mode(0, 1)
      self.pause(_state)
      self.set_position()
      return True

  def player_backward(self, _int):
    if self.get_play_mode(1):
      pos_int = self.get_position()
      if pos_int > 0:
        seek_nanos = pos_int - _int
        if seek_nanos < 0: seek_nanos = 0
        self.seek(seek_nanos)
        GLib.timeout_add(200, self.seek_update)

  def player_forward(self, _int):
    if self.get_play_mode(1):
      pos_int = self.get_position()
      if pos_int > 0:
        seek_nanos = pos_int + _int
        if seek_nanos > self.duration:
          self.set_end_of_stream()
        else:
          self.seek(seek_nanos)
          GLib.timeout_add(200, self.seek_update)

  def set_position(self):
    if not self.timeout_set_pos: return
    if self.duration:
      if self.duration > 0:
        position = self.get_position()
        self.update_time_labels(position)
        self.seek_bool = False
        if self.update_adj:
          self.adj.set_value(float((position * 100) / self.duration))
        self.seek_bool = True
      if not (self.get_end_of_stream() or self.duration < 0):
        return True
    self.set_end_of_stream()

  def set_duration(self):
    if self.timeout_set_pos:
      if self.get_play_mode(1):
        self.duration = self.get_duration()
        if self.duration > 0:
          self.dur_str = " / " + gst_util.convert_nanos(self.duration)
          self.set_all()
          return
      return True

  def set_end_of_stream(self):
    self.window.lookup_action("end-of-stream").activate()

  def update_time_labels(self, _pos):
    time_str = gst_util.convert_nanos(_pos) + self.dur_str
    self.time_label_0.set_text(time_str)
    self.time_label_1.set_text(time_str)

  def get_media_title(self):
    return self.media_title

  def hscale_release(self, controller, button=None, _1=None, _2=None, _3=None):
    if self.get_play_mode(1) and self.seek_bool and self.duration:
      self.update_adj = False
      self.seek(self.duration * (self.adj.get_value() / 100))
      GLib.timeout_add(200, self.seek_update)

  def seek_update(self):
      self.set_position()
      self.update_adj = True

  def player_window_scroll(self, controller, _0=None, _1=None, _2=None):
    if _1 < -0.4:
      self.player_forward(self.forward_scroll)
    elif _1 > 0.4:
      self.player_backward(self.backward_scroll)

  def button_callback(self, controller, npress, _1, _2):
    _button = controller.get_current_button()
    if _button == 1:
      if npress == 1:
        self.button_pause_bool = True
        GLib.timeout_add(390, self.button_pause)
      elif npress == 2:
        self.button_pause_bool = False
        if self.window.get_property("fullscreened"):
          self.window.lookup_action("fullscreen").change_state(GLib.Variant.new_boolean(False))
        else:
          self.window.lookup_action("fullscreen").change_state(GLib.Variant.new_boolean(True))
    elif _button == 8:
      self.player_backward(self.backward_side)
    elif _button == 9:
      self.player_forward(self.forward_side)

  def button_pause(self, _1=None):
    if self.button_pause_bool:
      self.window.lookup_action("pause").activate()

  def cursor_show(self, controller, _x=None, _y=None, _2=None):
    if controller.get_property("is-pointer") and self.pointer_x != _x:
      self.view.set_cursor_from_name("default")
      if self.window.get_property("fullscreened") and not self.switch_to_fullscreen:
        self.popup_show(True)
      if not self.timeout_cursor:
        self.timeout_cursor = GLib.timeout_add(self.popup_time/5, self.cursor_hide)
      self.cursor_int = 0
      self.pointer_x = _x

  def cursor_hide(self, _1=None):
    self.cursor_int +=1
    if self.cursor_int > 4:
      self.timeout_cursor = None
      if not self.window.get_property("fullscreened"):
        self.view.set_cursor_from_name("none")
        self.popup_show(False)
      else:
        if not self.popup_has_pointer:
          self.popup_show(False)
          self.view.set_cursor_from_name("none")
    else:
      return True

  def popup_show(self, _bool=False):
    self.popup.set_visible(_bool)

  def popup_enter(self, _1, _2=None, _3=None):
    self.popup_has_pointer = True

  def popup_leave(self, _1, _2=None, _3=None):
    self.popup_has_pointer = False

  def set_switch_to_fullscreen(self, _bool=False):
    self.switch_to_fullscreen = _bool

  def key_press(self, _1, _keyval, _3=None, _4=None):
    if _keyval == 32:
      self.window.lookup_action("pause").activate()
    elif _keyval == 65361:
      self.player_backward(self.backward_seek)
    elif _keyval == 65363:
      self.player_forward(self.forward_seek)
    elif _keyval == 65362:
      self.player_forward(self.forward_seek_long)
    elif _keyval == 65364:
      self.player_backward(self.backward_seek_long)
    elif _keyval == 65307:
      if self.window.get_property("fullscreened"):
        self.window.lookup_action("fullscreen").change_state(GLib.Variant.new_boolean(False))

  def set_media_info(self):
    if self.media_title:
      self.title.set_text(self.media_title)
    else:
      self.title.set_text(self.file_name)
    self.info_view.get_buffer().set_text(self.media_info)

  def set_video_list(self):
    self.video_view.remove_all()
    for i in self.video_list:
      self.video_view.append(Gtk.Label.new(i))

  def set_audio_list(self):
    self.audio_view.remove_all()
    for i in self.audio_list:
      self.audio_view.append(Gtk.Label.new(i))

  def set_sub_list(self):
    self.sub_view.remove_all()
    if len(self.sub_list) < self.get_sub_tot():
      gst_util.add_sub(self.sub_list)
    for i in self.sub_list:
      self.sub_view.append(Gtk.Label.new(i))

  def set_all(self):
    self.set_media_info()
    self.set_video_list()
    self.set_audio_list()
    self.set_sub_list()
    self.update_time_labels(0)

  def get_play_mode(self, _mode=0):
    return self.play_mode[_mode]

  def set_play_mode(self, _mode, _val):
    self.play_mode[_mode] = _val

  def disc_callback(self, _disc, _info, _err):
    if not _err and self.get_play_mode() > 0:
      self.play(self.file_path, self.sub_path)
      self.media_info, self.media_title = gst_util.process_disc_info(_info, self.video_list, self.audio_list, self.sub_list)
      self.set_play_mode(1, 1)

