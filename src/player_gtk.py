import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

class Main:
  def __init__(self):
    self.player = Gtk.MediaFile.new()
    self.view = Gtk.Picture.new_for_paintable(self.player)

  def stop(self):
    if self.player.is_prepared():
      self.player.set_playing(False)
      self.player.stream_unprepared()
    self.player.clear()
    self.view.set_paintable(None)

  def play(self, file_path, sub_path):
    self.player.set_filename(file_path)
    self.player.set_playing(True)

  def pause(self, _bool):
    self.player.set_playing(not _bool)

  def seek(self, position):
    self.player.seek(position / 1000)

  def get_position(self):
    return self.player.get_property("timestamp") * 1000

  def get_duration(self):
    if self.player.has_video(): self.view.set_paintable(self.player)
    return self.player.get_property("duration") * 1000

  def get_end_of_stream(self):
    return self.player.get_ended()

  def show_subtitle(self, _bool):
    pass

  def set_subtitle(self, _int):
    pass

  def get_subtitle(self):
    return 0

  def get_sub_tot(self):
    return 0

  def set_mute(self, _bool):
    self.player.set_muted(_bool)

  def set_audio(self, _int):
    pass

  def set_video(self, _int):
    pass

  def force_software(self, _bool):
    pass

