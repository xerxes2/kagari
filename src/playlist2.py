import sys, os, gi
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gdk', '4.0')
from gi.repository import Gdk
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

class Main:
  def __init__(self, window, config, Player):
    self.window = window
    self.config = config
    self.Player = Player
    self.current_pos = None
    self.current_mode = 0
    self.timeout_id = None
    self.formats = tuple(self.config.options("formats"))
    _action = Gio.SimpleAction.new("end-of-stream", None)
    _action.connect("activate", self.play_next_end)
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("add-new-file", GLib.VariantType.new("s"))
    _action.connect("activate", self.add_to_playlist)
    self.window.add_action(_action)

    self.stringlist = Gtk.StringList.new()
    self.selection = Gtk.SingleSelection.new(self.stringlist)
    self.factory = Gtk.SignalListItemFactory.new()
    self.factory.connect("setup", self.factory_setup)
    self.factory.connect("bind", self.factory_bind)
    self.view = Gtk.ScrolledWindow.new()
    self.listview = Gtk.ListView.new(self.selection, self.factory)
    self.listview.connect("activate", self.listview_callback)
    self.view.set_child(self.listview)
    self.listview.set_margin_bottom(8)
    _event = Gtk.GestureClick.new()
    _event.connect("pressed", self.button_press)
    _event.set_button(0)
    self.listview.add_controller(_event)
    _event = Gtk.EventControllerKey.new()
    _event.connect("key-pressed", self.key_press)
    self.listview.add_controller(_event)

    self.menu = Gio.Menu.new()
    _action = Gio.SimpleAction.new("playlist-remove", None)
    _action.connect("activate", self.options, "remove")
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("playlist-move-up", None)
    _action.connect("activate", self.options, "move-up")
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("playlist-move-down", None)
    _action.connect("activate", self.options, "move-down")
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("playlist-move-first", None)
    _action.connect("activate", self.options, "move-first")
    self.window.add_action(_action)
    _action = Gio.SimpleAction.new("playlist-move-last", None)
    _action.connect("activate", self.options, "move-last")
    self.window.add_action(_action)
    self.menu.append("Remove", "win.playlist-remove")
    self.menu.append("Move Up", "win.playlist-move-up")
    self.menu.append("Move Down", "win.playlist-move-down")
    self.menu.append("Move to First", "win.playlist-move-first")
    self.menu.append("Move to Last", "win.playlist-move-last")
    self.popover = Gtk.PopoverMenu.new_from_model(self.menu)
    self.popover.set_parent(self.listview)

  def factory_setup(self, _factory, _item):
    _hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 10)
    _hbox.set_margin_start(10)
    _image = Gtk.Image.new_from_icon_name(None)
    _hbox.append(_image)
    _label = Gtk.Label.new("")
    _hbox.append(_label)
    _item.set_child(_hbox)

  def factory_bind(self, _factory, _item):
    if _item.get_position() == self.current_pos: _current = True
    else: _current = False
    _path = _item.get_item().get_string()
    _hbox = _item.get_child()
    _image = _hbox.get_first_child()
    _label = _hbox.get_last_child()
    if _current and self.current_mode == 1:
      _image.set_from_icon_name("media-playback-start")
    elif _current and self.current_mode == 2:
      _image.set_from_icon_name("media-playback-pause")
    else:
      _image.set_from_icon_name(None)
    _label.set_text(os.path.basename(_path))

  def listview_callback(self, _view, _pos):
    self.play_selected()

  def add_item(self, _path):
    self.stringlist.append(_path)

  def remove_item(self, _pos):
    self.stringlist.remove(_pos)

  def move_item(self, _pos0, _pos1):
    _item0 =  self.get_path(_pos0)
    self.stringlist.remove(_pos0)
    self.stringlist.splice(_pos1, 0, [_item0])

  def modify_item(self, _pos, _str):
    self.stringlist.splice(_pos, 1, [_str])

  def set_current_mode(self, _mode):
    if self.current_pos != None:
      self.current_mode = _mode
      self.modify_item(self.current_pos, self.get_path(self.current_pos))

  def get_path(self, _pos):
    if _pos != None: return self.stringlist.get_string(_pos)

  def remove_all_items(self):
    for i in range(self.get_playlist_size()):
      self.stringlist.remove(0)

  def get_playlist_size(self):
    return len(self.stringlist)

  def get_selected(self):
    return self.selection.get_selected()

  def check_path(self, _path):
    if _path and os.path.isfile(_path) and _path.endswith(self.formats): return True

  def button_press(self, gesture, npress, _x=None, _y=None):
    if npress == 2: pass
    elif gesture.get_current_button() == 3:
      self.popover.popup()
      _rect = Gdk.Rectangle()
      _rect.x = _x
      _rect.y = _y
      self.popover.set_property("pointing-to", _rect)

  def key_press(self, _event, _keyval, _keycode, _flags):
    if _keyval == 65535: self.options(None, None, "remove")
    elif _keyval == 65365: self.options(None, None, "move-up")
    elif _keyval == 65366: self.options(None, None, "move-down")
    elif _keyval == 65360: self.options(None, None, "move-first")
    elif _keyval == 65367: self.options(None, None, "move-last")

  def options(self, _action, _2, _var):
    if _var == "move-up":
      _pos = self.get_selected()
      _size = self.get_playlist_size()
      if _size and _pos > 0:
        if self.current_pos != None:
          if self.current_pos == _pos:
            self.current_pos = _pos - 1
          elif self.current_pos == _pos - 1:
            self.current_pos = _pos
        self.move_item(_pos, _pos - 1)
    elif _var == "move-down":
      _pos = self.get_selected()
      _size = self.get_playlist_size()
      if _size and _pos < (_size - 1):
        if self.current_pos != None:
          if self.current_pos == _pos:
            self.current_pos = _pos + 1
          elif self.current_pos == _pos + 1:
            self.current_pos = _pos
        self.move_item(_pos, _pos + 1)
    elif _var == "move-first":
      _pos = self.get_selected()
      _size = self.get_playlist_size()
      if _size and _pos > 0:
        if self.current_pos != None:
          if self.current_pos == _pos:
            self.current_pos = 0
          elif self.current_pos < _pos:
            self.current_pos +=1
        self.move_item(_pos, 0)
    elif _var == "move-last":
      _pos = self.get_selected()
      _size = self.get_playlist_size()
      if _size and _pos < _size - 1:
        if self.current_pos != None:
          if self.current_pos == _pos:
            self.current_pos = _size - 1
          elif self.current_pos > _pos:
            self.current_pos -=1
        self.move_item(_pos, _size - 1)
    elif _var == "remove":
      _pos = self.get_selected()
      if self.get_playlist_size():
        if self.current_pos != None:
          if self.current_pos == _pos:
            self.playlist_stop()
            self.current_pos = None
          elif self.current_pos > _pos:
            self.current_pos -=1
        self.remove_item(_pos)

  def playlist_stop(self):
    if self.timeout_id:
      GLib.source_remove(self.timeout_id)
      self.timeout_id = None
    self.Player.player_stop()
    self.set_current_mode(0)

  def playlist_play(self, sub_path=""):
    self.Player.player_play(self.get_path(self.current_pos), sub_path)
    self.set_current_mode(1)

  def playlist_pause(self, _bool):
    if self.Player.player_pause(_bool):
      self.set_current_mode(self.Player.get_play_mode())
      return True

  def play_previous(self):
    if self.Player.get_play_mode() > 0 or self.timeout_id:
      if self.Player.get_play_mode() > 0: self.playlist_stop()
      else: self.set_current_mode(0)
      if self.current_pos > 0: self.current_pos -=1
      self.set_current_mode(1)
      self.play_thread_int = 0
      if not self.timeout_id:
        self.timeout_id = GLib.timeout_add(200, self.play_thread)

  def play_next(self):
    if self.Player.get_play_mode() > 0 or self.timeout_id:
      if self.Player.get_play_mode() > 0: self.playlist_stop()
      else: self.set_current_mode(0)
      if self.current_pos < (self.get_playlist_size() - 1): self.current_pos +=1
      self.set_current_mode(1)
      self.play_thread_int = 0
      if not self.timeout_id:
        self.timeout_id = GLib.timeout_add(200, self.play_thread)

  def play_thread(self):
    self.play_thread_int +=1
    if self.play_thread_int > 3:
      self.set_and_play(self.current_pos)
      self.timeout_id = None
      return
    return True

  def play_next_end(self, _a=None, _w=None):
    self.playlist_stop()
    if self.current_pos < (self.get_playlist_size() -1):
      self.current_pos +=1
      self.play_thread_int = 0
      self.timeout_id = GLib.timeout_add(200, self.play_thread)

  def play_selected(self):
    self.set_and_play(self.get_selected())

  def set_and_play(self, _pos):
    self.playlist_stop()
    _size = self.get_playlist_size()
    if _size > _pos:
      for i in range(_size - _pos):
        _path = self.get_path(_pos)
        if self.check_path(_path):
          self.current_pos = _pos
          self.playlist_play()
          return
        _pos +=1

  def set_subtitle_uri(self, sub_path):
    if self.Player.get_play_mode() > 0:
      self.playlist_stop()
      self.playlist_play(sub_path)
    else:
      _pos = self.get_selected()
      _path = self.get_path(_pos)
      if self.check_path(_path):
        self.current_pos = _pos
        self.playlist_play(sub_path)

  def add_to_playlist(self, _action, _var):
    self.add_to_playlist_from_path(_var.get_string())

  def add_to_playlist_from_path(self, _path):
    if self.check_path(_path):
      self.add_item(_path)
      if self.Player.get_play_mode() == 0:
        self.set_and_play(self.get_playlist_size() - 1)

  def clear_playlist(self):
    self.playlist_stop()
    self.remove_all_items()
    self.current_pos = None

  def load_playlist(self, _path):
    _file = open(_path, "r")
    _str = _file.read()
    _file.close()
    _list = _str.splitlines()
    for i in _list:
      self.add_to_playlist_from_path(i)

  def save_playlist(self, _path):
    _file = open(_path, "w")
    for i in range(self.get_playlist_size()):
      _file.write(self.get_path(i) + "\n")
    _file.close()

  def load_folder(self, _path):
    _list = os.listdir(_path)
    _list.sort()
    for i in _list:
      self.add_to_playlist_from_path(_path + "/" + i)

