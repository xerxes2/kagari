import os, gi
from gi.repository import GLib
from gi.repository import Gio
gi.require_version('Gdk', '4.0')
from gi.repository import Gdk
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

class Player_Menu:
  def __init__(self, window, config, Playlist,  Player, Fileselect, menu_bar, notebook,
               control_box, toolbar_0, toolbar_1, info_view, video_view, audio_view, sub_view):
    self.window = window
    self.config = config
    self.Playlist = Playlist
    self.Player = Player
    self.Fileselect = Fileselect
    self.notebook_file = notebook
    self.menu_bar = menu_bar
    self.control_box = control_box
    self.toolbar_0 = toolbar_0
    self.toolbar_1 = toolbar_1

    self.forward_seek = self.config.getint("options", "forward") * 1000000000
    self.backward_seek = self.config.getint("options", "backward") * 1000000000

    self.popover_info = Gtk.Window.new()
    self.popover_info.set_title("Media Info")
    self.popover_info.set_parent(self.window)
    self.popover_info.set_default_size(500, 600)
    self.popover_info.connect("close-request", self.widget_set_visible)
    _scrollw = Gtk.ScrolledWindow.new()
    _scrollw.set_child(info_view)
    self.popover_info.set_child(_scrollw)

    self.popover_stream = Gtk.Window.new()
    self.popover_stream.set_title("Stream Selector")
    self.popover_stream.set_parent(self.window)
    self.popover_stream.set_default_size(300, 500)
    self.popover_stream.connect("close-request", self.widget_set_visible)
    self.notebook_stream = Gtk.Notebook()
    self.notebook_stream.set_can_focus(False)
    self.popover_stream.set_child(self.notebook_stream)
    _scrollw = Gtk.ScrolledWindow.new()
    _scrollw.set_child(video_view)
    self.notebook_stream.append_page(_scrollw, Gtk.Label.new("Video"))
    _scrollw = Gtk.ScrolledWindow.new()
    _scrollw.set_child(audio_view)
    self.notebook_stream.append_page(_scrollw, Gtk.Label.new("Audio"))
    _scrollw = Gtk.ScrolledWindow.new()
    _scrollw.set_child(sub_view)
    self.notebook_stream.append_page(_scrollw, Gtk.Label.new("Subtitle"))

    video_view.connect("row-selected", self.set_video)
    audio_view.connect("row-selected", self.set_audio)
    sub_view.connect("row-selected", self.set_subtitle)
    self.file_dialog = Gtk.FileDialog.new()
    self.gfile_playlist = Gio.file_new_for_path(os.path.expanduser("~/.config/kagari/playlists"))
    self.gfile_home = Gio.file_new_for_path(os.path.expanduser("~"))

    action = Gio.SimpleAction.new("clear-playlist", None)
    action.connect("activate", self.playlist_menu)
    self.window.add_action(action)
    action = Gio.SimpleAction.new("load-playlist", None)
    action.connect("activate", self.playlist_menu)
    self.window.add_action(action)
    action = Gio.SimpleAction.new("save-playlist", None)
    action.connect("activate", self.playlist_menu)
    self.window.add_action(action)
    action = Gio.SimpleAction.new_stateful('dot-files', None, GLib.Variant.new_boolean(self.config.getboolean("options", "dot-files")))
    action.connect("change-state", self.file_menu)
    self.window.add_action(action)

    action = Gio.SimpleAction.new("open-file", None)
    action.connect("activate", self.file_menu)
    self.window.add_action(action)
    action = Gio.SimpleAction.new("load-folder", None)
    action.connect("activate", self.file_menu)
    self.window.add_action(action)
    action = Gio.SimpleAction.new("folder-root", None)
    action.connect("activate", self.file_menu)
    self.window.add_action(action)
    action = Gio.SimpleAction.new("folder-home", None)
    action.connect("activate", self.file_menu)
    self.window.add_action(action)

    _menu = Gio.Menu.new()
    _menu.append("Home", "win.folder-home")
    _menu.append("/", "win.folder-root")
    menu = Gio.Menu.new()
    menu.append("Open File", "win.open-file")
    menu.append("Load Folder", "win.load-folder")
    menu.append("Dot Files", "win.dot-files")
    menu.append_submenu("Folder", _menu)
    menu.append("Quit", "win.quit")
    self.menu_bar.append_submenu("File", menu)

    menu_view = Gio.Menu.new()
    self.menu_bar.append_submenu("View", menu_view)
    action = Gio.SimpleAction.new("media-info", None)
    action.connect("activate", self.view_menu)
    self.window.add_action(action)
    menu_view.append("Media Info", "win.media-info")
    _action = Gio.SimpleAction.new_stateful("playlist", None, GLib.Variant.new_boolean(self.config.getboolean("options", "show-file")))
    _action.connect("change-state", self.view_menu)
    self.window.add_action(_action)
    menu_view.append("Playlist", "win.playlist")
    action = Gio.SimpleAction.new_stateful("fullscreen", None, GLib.Variant.new_boolean(False))
    action.connect("change-state", self.view_menu)
    self.window.add_action(action)
    menu_view.append("Fullscreen", "win.fullscreen")

    menu_video = Gio.Menu.new()
    self.menu_bar.append_submenu("Video", menu_video)
    _action = Gio.SimpleAction.new_stateful("force-software", None, GLib.Variant.new_boolean(False))
    _action.connect("change-state", self.video_menu)
    self.window.add_action(_action)
    menu_video.append("Force Software", "win.force-software")
    action = Gio.SimpleAction.new("set-video", None)
    action.connect("activate", self.video_menu)
    self.window.add_action(action)
    menu_video.append("Set Video", "win.set-video")

    menu_audio = Gio.Menu.new()
    self.menu_bar.append_submenu("Audio", menu_audio)
    _action = Gio.SimpleAction.new_stateful("mute", None, GLib.Variant.new_boolean(False))
    _action.connect("change-state", self.audio_menu)
    self.window.add_action(_action)
    menu_audio.append("Mute", "win.mute")
    action = Gio.SimpleAction.new("set-audio", None)
    action.connect("activate", self.audio_menu)
    self.window.add_action(action)
    menu_audio.append("Set Audio", "win.set-audio")

    menu_sub = Gio.Menu.new()
    self.menu_bar.append_submenu("Subtitle", menu_sub)
    _action = Gio.SimpleAction.new_stateful("show-sub", None, GLib.Variant.new_boolean(self.config.getboolean("options", "show-sub")))
    _action.connect("change-state", self.sub_menu)
    self.window.add_action(_action)
    menu_sub.append("Show Subtitle", "win.show-sub")
    action = Gio.SimpleAction.new("set-sub", None)
    action.connect("activate", self.sub_menu)
    self.window.add_action(action)
    menu_sub.append("Set Subtitle", "win.set-sub")
    action = Gio.SimpleAction.new("set-sub-uri", None)
    action.connect("activate", self.sub_menu)
    self.window.add_action(action)
    menu_sub.append("Set Subtitle File", "win.set-sub-uri")

    _menu = Gio.Menu.new()
    _menu.append("Clear Playlist", "win.clear-playlist")
    _menu.append("Load Playlist", "win.load-playlist")
    _menu.append("Save Playlist", "win.save-playlist")
    self.menu_bar.append_submenu("Playlist", _menu)

    player_menu = Gio.Menu.new()
    self.menu_bar.append_submenu("Playback", player_menu)

    action = Gio.SimpleAction.new('stop', None)
    action.connect("activate", self.playback_menu)
    player_menu.append("Stop", "win.stop")
    self.window.add_action(action)
    button = Gtk.Button.new_with_label("Stop")
    button.set_action_name("win.stop")
    button.set_icon_name("media-playback-stop")
    self.toolbar_0.append(button)

    action = Gio.SimpleAction.new('play', None)
    action.connect("activate", self.playback_menu)
    player_menu.append("Play", "win.play")
    self.window.add_action(action)
    button = Gtk.Button.new_with_label("Play")
    button.set_action_name("win.play")
    button.set_icon_name("media-playback-start")
    self.toolbar_0.append(button)

    self.pause = Gio.SimpleAction.new_stateful('pause', None, GLib.Variant.new_boolean(False))
    self.pause.connect("change-state", self.playback_menu)
    player_menu.append("Pause", "win.pause")
    self.window.add_action(self.pause)
    button = Gtk.ToggleButton.new_with_label("Pause")
    button.set_action_name("win.pause")
    button.set_icon_name("media-playback-pause")
    self.toolbar_0.append(button)

    action = Gio.SimpleAction.new('backward', None)
    action.connect("activate", self.playback_menu)
    player_menu.append("Backward", "win.backward")
    self.window.add_action(action)
    button = Gtk.Button.new_with_label("Backward")
    button.set_action_name("win.backward")
    button.set_icon_name("media-seek-backward")
    self.toolbar_0.append(button)

    action = Gio.SimpleAction.new('forward', None)
    action.connect("activate", self.playback_menu)
    player_menu.append("Forward", "win.forward")
    self.window.add_action(action)
    button = Gtk.Button.new_with_label("Forward")
    button.set_action_name("win.forward")
    button.set_icon_name("media-seek-forward")
    self.toolbar_0.append(button)

    action = Gio.SimpleAction.new('previous', None)
    action.connect("activate", self.playback_menu)
    player_menu.append("Previous", "win.previous")
    self.window.add_action(action)
    button = Gtk.Button.new_with_label("Previous")
    button.set_action_name("win.previous")
    button.set_icon_name("media-skip-backward")
    self.toolbar_0.append(button)

    action = Gio.SimpleAction.new('next', None)
    action.connect("activate", self.playback_menu)
    player_menu.append("Next", "win.next")
    self.window.add_action(action)
    button = Gtk.Button.new_with_label("Next")
    button.set_action_name("win.next")
    button.set_icon_name("media-skip-forward")
    self.toolbar_0.append(button)

    button = Gtk.Button.new_with_label("Stop")
    button.set_action_name("win.stop")
    button.set_icon_name("media-playback-stop")
    self.toolbar_1.append(button)

    button = Gtk.ToggleButton.new_with_label("Pause")
    button.set_action_name("win.pause")
    button.set_icon_name("media-playback-pause")
    self.toolbar_1.append(button)

  def file_menu(self, _action, _state):
    _name = _action.get_name()
    if _name == "load-folder":
      self.file_dialog.set_initial_folder(self.gfile_home)
      self.file_dialog.select_folder(self.window, None, self.load_folder)
    elif _name == "open-file":
      self.file_dialog.set_initial_folder(self.gfile_home)
      self.file_dialog.open(self.window, None, self.open_file)
    elif _name == "dot-files":
      _action.set_state(_state)
      self.Fileselect.fileselect_reset(self.config.get("options", "startup-dir"))
    elif _name == "folder-home":
      self.Fileselect.fileselect_reset(os.environ['HOME'])
    elif _name == "folder-root":
      self.Fileselect.fileselect_reset("/")

  def video_menu(self, _action, _state):
    _name = _action.get_name()
    if _name == "force-software":
      _action.set_state(_state)
      self.Player.force_software(_state)
    elif _name == "set-video":
      self.notebook_stream.set_current_page(0)
      self.popover_stream.set_visible(True)

  def audio_menu(self, _action, _state):
    _name = _action.get_name()
    if _name == "mute":
      _action.set_state(_state)
      self.Player.set_mute(_state)
    elif _name == "set-audio":
      self.notebook_stream.set_current_page(1)
      self.popover_stream.set_visible(True)

  def sub_menu(self, _action, _state=None):
    _name = _action.get_name()
    if _name == "show-sub":
      _action.set_state(_state)
      self.Player.show_subtitle(_state)
    elif _name == "set-sub":
      self.notebook_stream.set_current_page(2)
      self.popover_stream.set_visible(True)
    elif _name == "set-sub-uri":
      self.file_dialog.set_initial_folder(self.gfile_home)
      self.file_dialog.open(self.window, None, self.set_sub_uri)

  def view_menu(self, _action, _state=None):
    _name = _action.get_name()
    if _name == "media-info":
      self.popover_info.set_visible(True)
    elif _name == "playlist":
      _action.set_state(_state)
      self.notebook_file.set_visible(_state)
      if _state: self.notebook_file.set_current_page(1)
    elif _name == "fullscreen":
      _action.set_state(_state)
      if _state:
        self.Player.set_switch_to_fullscreen(True)
        self.window.fullscreen()
        self.control_box.set_visible(False)
        self.notebook_file.set_visible(False)
        self.Player.view.grab_focus()
        GLib.timeout_add(100, self.Player.set_switch_to_fullscreen)
      else:
        self.window.unfullscreen()
        self.control_box.set_visible(True)
        if self.window.lookup_action("playlist").get_state():
          self.notebook_file.set_visible(True)
        GLib.timeout_add(15, self.Player.popup_show)

  def playlist_menu(self, _action, _state):
    _name = _action.get_name()
    if _name == "clear-playlist":
      self.Playlist.clear_playlist()
    elif _name == "load-playlist":
      self.file_dialog.set_initial_folder(self.gfile_playlist)
      self.file_dialog.open(self.window, None, self.load_playlist)
    elif _name == "save-playlist":
      self.file_dialog.set_initial_folder(self.gfile_playlist)
      self.file_dialog.save(self.window, None, self.save_playlist)

  def playback_menu(self, _action, _state=None):
    _name = _action.get_name()
    if _name == "stop":
      self.Playlist.playlist_stop()
    elif _name == "play":
      self.Playlist.play_selected()
    elif _name == "pause":
      if self.Playlist.playlist_pause(_state):
        _action.set_state(_state)
    elif _name == "backward":
      self.Player.player_backward(self.backward_seek)
    elif _name == "forward":
      self.Player.player_forward(self.forward_seek)
    elif _name == "previous":
      self.Playlist.play_previous()
    elif _name == "next":
      self.Playlist.play_next()

  def widget_set_visible(self, _w, _bool=False):
    _w.set_visible(_bool)
    return True

  def set_video(self, _w, _r):
    _int = -1
    if _r: _int = _r.get_index()
    self.Player.set_video(_int)

  def set_audio(self, _w, _r):
    _int = -1
    if _r: _int = _r.get_index()
    self.Player.set_audio(_int)

  def set_subtitle(self, _w, _r):
    _int = -1
    if _r: _int = _r.get_index()
    self.Player.set_subtitle(_int)

  def set_sub_uri(self, _dialog, _task):
    if not _task.had_error():
      self.Playlist.set_subtitle_uri(_dialog.open_finish(_task).get_path())

  def load_playlist(self, _dialog, _task):
    if not _task.had_error():
      self.Playlist.load_playlist(_dialog.open_finish(_task).get_path())

  def save_playlist(self, _dialog, _task):
    if not _task.had_error():
      self.Playlist.save_playlist(_dialog.save_finish(_task).get_path())

  def load_folder(self, _dialog, _task):
    if not _task.had_error():
      self.Playlist.load_folder(_dialog.select_folder_finish(_task).get_path())

  def open_file(self, _dialog, _task):
    if not _task.had_error():
      self.Playlist.add_to_playlist_from_path(_dialog.open_finish(_task).get_path())

