import sys, os, gi, configparser, shutil
from gi.repository import Gio
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk
from . import fileselect
from . import playlist2 as playlist
from . import player_menu
from . import player

def app_init_src(file_path):
  global configpath
  configpath = os.getcwd() + "/src/data/"
  application = Gtk.Application().new(None, 0)
  application.connect("activate", GTK_Main, file_path)
  application.run()

def app_init(file_path):
  global configpath
  configpath = os.environ['HOME'] + "/.config/kagari/"
  if not os.path.isdir(configpath):
    os.makedirs(configpath + "playlists")
  if not os.path.isfile(configpath + "settings.conf"):
    for i in sys.path:
      _path = i + "/kagari/data/settings.conf"
      if os.path.isfile(_path):
        shutil.copy(_path, configpath)
        break
  application = Gtk.Application().new(None, 0)
  application.connect("activate", GTK_Main, file_path)
  application.run()

class GTK_Main:
  def __init__(self, _app, file_path):
    self.application = _app
    self.config = configparser.ConfigParser(allow_no_value=True)
    self.config.read([configpath + "settings.conf"])
    self.window = Gtk.ApplicationWindow.new(self.application)
    self.window.set_default_size(self.config.getint("options", "window-width"), self.config.getint("options", "window-height"))
    menu_bar = Gio.Menu.new()
    headerbar = Gtk.HeaderBar.new()
    self.window.set_titlebar(headerbar)
    vbox_main = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
    control_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
    self.window.set_child(vbox_main)
    self.window.connect("destroy", self.app_quit)
    self.window.connect("close-request", self.app_quit)
    self.window.set_title("Kagari")
    action = Gio.SimpleAction.new("quit", None)
    action.connect("activate", self.app_quit)
    self.window.add_action(action)

    action = Gio.SimpleAction.new("open-menu", None)
    action.connect("activate", self.menu_open)
    self.window.add_action(action)
    button = Gtk.Button.new_with_label("Open Menu")
    button.set_action_name("win.open-menu")
    button.set_icon_name("open-menu-symbolic")
    button.set_margin_end(10)
    headerbar.pack_end(button)
    self.popover_menu = Gtk.PopoverMenu.new_from_model(menu_bar)
    self.popover_menu.set_parent(button)

    self.title = Gtk.Label.new("")
    self.title.set_hexpand(True)
    self.notebook = Gtk.Notebook()
    self.notebook.set_size_request(self.config.getint("options", "note-width"), -1)
    self.hpaned = Gtk.Paned().new(Gtk.Orientation.HORIZONTAL)
    self.hpaned.set_vexpand(True)
    self.Fileselect = fileselect.Fileselect(self.window)

    _hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
    toolbar_0 = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
    toolbar_1 = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
    time_label_0 = Gtk.Label.new()
    time_label_1 = Gtk.Label.new()
    _hbox.append(toolbar_0)
    _hbox.append(self.title)
    control_box.append(_hbox)
    time_hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
    control_box.append(time_hbox)

    adj = Gtk.Adjustment.new(0.0, 0.0, 101.0, 1.0, 1.0, 1.0)
    hscale_0 = Gtk.Scale.new(Gtk.Orientation.HORIZONTAL, None)
    hscale_0.set_adjustment(adj)
    hscale_1 = Gtk.Scale.new(Gtk.Orientation.HORIZONTAL, None)
    hscale_1.set_adjustment(adj)

    hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
    hbox.append(toolbar_1)
    hbox.append(time_label_1)
    self.center_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 5)
    self.center_box.set_size_request(self.config.getint("options", "popup-width"), -1)
    self.center_box.set_valign(Gtk.Align.END)
    self.center_box.set_halign(Gtk.Align.CENTER)
    self.center_box.append(hscale_1)
    self.center_box.append(hbox)
    self.info_view = Gtk.TextView.new()
    self.info_view.set_editable(False)
    self.info_view.set_cursor_visible(False)
    self.video_view = Gtk.ListBox.new()
    self.audio_view = Gtk.ListBox.new()
    self.sub_view = Gtk.ListBox.new()

    sys.path.insert(0, os.path.dirname(__file__))
    from importlib import import_module
    backend = import_module("player_" + self.config.get("options", "backend"))
    MyPlayer = get_class([player.Main, backend.Main])
    self.Player = MyPlayer(self.window, self.config, time_label_0, time_label_1, adj, self.center_box, self.info_view,
      self.video_view, self.audio_view, self.sub_view, self.title)
    self.Playlist = playlist.Main(self.window, self.config, self.Player)
    self.Menu = player_menu.Player_Menu(self.window, self.config, self.Playlist, self.Player, self.Fileselect, menu_bar,
      self.notebook, control_box, toolbar_0, toolbar_1, self.info_view, self.video_view, self.audio_view, self.sub_view)

    self.Fileselect.fileselect_reset(self.config.get("options", "startup-dir"))
    time_label_0.set_size_request(self.config.getint("options", "time-width"), -1)
    time_label_0.set_text("00:00 / 00:00")
    time_label_1.set_size_request(self.config.getint("options", "time-width"), -1)
    time_label_1.set_text("00:00 / 00:00")
    self.player_window = self.Player.view
    self.player_window.set_hexpand(True)
    self.player_window.set_vexpand(True)
    overlay = Gtk.Overlay.new()
    overlay.set_child(self.player_window)
    overlay.add_overlay(self.center_box)
    overlay.set_size_request(self.config.getint("options", "window-width") - self.config.getint("options", "note-width"), -1)

    hscale_0.connect("value-changed", self.Player.hscale_release)
    hscale_0.set_value_pos(Gtk.PositionType.RIGHT)
    hscale_0.set_draw_value(False)
    hscale_0.set_digits(0)
    hscale_0.set_hexpand(True)
    time_hbox.append(time_label_0)
    time_hbox.append(hscale_0)

    hscale_1.connect("value-changed", self.Player.hscale_release)
    hscale_1.set_value_pos(Gtk.PositionType.RIGHT)
    hscale_1.set_draw_value(False)
    hscale_1.set_digits(0)

    self.notebook.append_page(self.Fileselect.view, Gtk.Label.new("Fileselector"))
    self.notebook.append_page(self.Playlist.view, Gtk.Label.new("Playlist"))
    self.hpaned.set_start_child(self.notebook)
    self.hpaned.set_end_child(overlay)
    vbox_main.append(control_box)
    vbox_main.append(self.hpaned)
    self.window.set_visible(True)
    self.center_box.set_visible(False)
    self.notebook.set_visible(self.window.lookup_action("playlist").get_state().get_boolean())

    _css = """.player-window {background-color: #000;}"""
    _provider = Gtk.CssProvider.new()
    _provider.load_from_string(_css)
    Gtk.StyleContext.add_provider_for_display(self.player_window.get_display(), _provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    self.player_window.add_css_class("player-window")
    _css = """.center-box {color: #eee; background-color: #111; padding: 10px;}"""
    _provider = Gtk.CssProvider.new()
    _provider.load_from_string(_css)
    Gtk.StyleContext.add_provider_for_display(self.center_box.get_display(), _provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    self.center_box.add_css_class("center-box")

    self.application.set_accels_for_action("win.fullscreen", [self.config.get("hotkeys", "fullscreen")])

    if file_path:
      if not file_path.startswith("/"):
        file_path = "{0}/{1}".format(os.getcwd(), file_path)
      self.Playlist.add_to_playlist_from_path(file_path)

  def menu_open(self, _0, _1):
    self.popover_menu.popup()

  def app_quit(self, _1, _2=None):
    self.window.lookup_action("stop").activate()
    self.application.quit()

def get_class(_list):
  class MyClass(*_list):
    pass
  return MyClass

